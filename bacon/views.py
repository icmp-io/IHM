from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse
from bacon.models import Probe

def home_page(request):

    if request.method == 'POST':
        ip_version = request.POST.get('ip_version', '')
        probe_type = request.POST.get('probe_type', '')
        target = request.POST.get('target', '')
        Probe.objects.create(ip_version=ip_version, probe_type=probe_type, target=target)
        return redirect('/')

    probes = Probe.objects.all()
    return render(request, 'home.html', {'probes':probes})

def view_probe(request, id):
    probe = get_object_or_404(Probe, pk=id)
    return render(request, 'probe.html', {'probe': probe})

