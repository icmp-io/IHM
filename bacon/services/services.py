import requests
import json

def get_key():
    url = 'http://dev.api.jambon.io/api/keys'
    r = requests.post(url)
    key = r.json()
    return key['data']

def create_probe(api_key,
                 ip_version="4",
                 type="icmp_echo",
                 target="google.com",
                 interval_s="30",
                 timeout_s="1",
                 settings=""):

    url = 'http://dev.api.jambon.io/api/probes'
    params = {'key':api_key}
    data = {
        'ip_version':ip_version,
        'type':type,
        'target':target,
        'interval_s':interval_s,
        'timeout_s':timeout_s,
        'settings':settings
    }
    r = requests.post(url, json={"probe":data}, params=params)
    return r.json()['data']['id']


def get_probe(id, api_key):
    url = 'http://dev.api.jambon.io/api/probe/' + str(id)
    params = {'key':api_key}
    r = requests.get(url, params=params)

    return r.json()['data']

def get_all_probes(api_key):
    url = 'http://dev.api.jambon.io/api/probes'
    params = {'key':api_key}
    r = requests.get(url, params=params)
    
    return r.json()['data']
