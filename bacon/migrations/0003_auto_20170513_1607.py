# -*- coding: utf-8 -*-
# Generated by Django 1.11rc1 on 2017-05-13 16:07
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bacon', '0002_auto_20170503_1010'),
    ]

    operations = [
        migrations.AlterField(
            model_name='probe',
            name='id',
            field=models.AutoField(primary_key=True, serialize=False),
        ),
    ]
