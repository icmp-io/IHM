from django.test import TestCase
from bacon.models import Probe

from ..services import services

        
class ServicesTest(TestCase):
    def test_can_get_a_key_from_jambon(self):
        key = services.get_key()['key']
        self.assertEqual(len(key), 36)

    def test_can_create_a_probe(self):

        api_key = services.get_key()['key']
        id = None

        id = services.create_probe(
            api_key=api_key,
            ip_version="4",
            type="icmp_echo",
            target="google.fr"
        )
        self.assertFalse(id is None)


    def test_can_fetch_a_probe(self):
        api_key = services.get_key()['key']
        id = services.create_probe(
            api_key=api_key,
            ip_version="4",
            type="icmp_echo",
            target="google.fr"
        )

        probe = services.get_probe(id, api_key)

        self.assertEqual(probe['id'], id)
        self.assertEqual(probe['ip_version'], 4)
        self.assertEqual(probe['type'], "icmp_echo")
        self.assertEqual(probe['target'], "google.fr")


    def test_can_fetch_all_probes_for_a_key(self):
        api_key1 = services.get_key()['key']
        api_key2 = services.get_key()['key']
        
        id1 = services.create_probe(
            api_key=api_key1,
            ip_version="4",
            type="icmp_echo",
            target="google.fr"
        )

        id2 = services.create_probe(
            api_key=api_key1,
            ip_version="4",
            type="icmp_echo",
            target="netflix.fr"
        )

        id3 = services.create_probe(
            api_key=api_key2,
            ip_version="4",
            type="icmp_echo",
            target="stackoverflow.com"
        )

        id4 = services.create_probe(
            api_key=api_key2,
            ip_version="4",
            type="icmp_echo",
            target="news.ycombinator.com"
        )

        probes_api_key1 = services.get_all_probes(api_key1)
        probes_api_key2 = services.get_all_probes(api_key2)
        self.assertEqual(len(probes_api_key1), 2)
        self.assertEqual(len(probes_api_key2), 2)

        ## Because the ordering might be altered since get_all_probes does not care about the
        ## creation order (might be due to the jambon api if the requests aren't processed
        ## sequentially but anyway..)
        probes_api_key1.sort(key = lambda x: x['id'])
        probes_api_key2.sort(key = lambda x: x['id'])
        
        probe1 = probes_api_key1[0]
        probe2 = probes_api_key1[1]
        probe3 = probes_api_key2[0]
        probe4 = probes_api_key2[1]
        
        self.assertEqual(probe1['id'], id1)
        self.assertEqual(probe1['ip_version'], 4)
        self.assertEqual(probe1['type'], "icmp_echo")
        self.assertEqual(probe1['target'], "google.fr")

        self.assertEqual(probe2['id'], id2)
        self.assertEqual(probe2['ip_version'], 4)
        self.assertEqual(probe2['type'], "icmp_echo")
        self.assertEqual(probe2['target'], "netflix.fr")

        self.assertEqual(probe3['id'], id3)
        self.assertEqual(probe3['ip_version'], 4)
        self.assertEqual(probe3['type'], "icmp_echo")
        self.assertEqual(probe3['target'], "stackoverflow.com")

        self.assertEqual(probe4['id'], id4)
        self.assertEqual(probe4['ip_version'], 4)
        self.assertEqual(probe4['type'], "icmp_echo")
        self.assertEqual(probe4['target'], "news.ycombinator.com")


