from django.core.urlresolvers import resolve
from django.test import TestCase
from django.http import HttpRequest 
from bacon.models import Probe

from bacon.views import home_page

class HomePageTest(TestCase):

    def test_uses_home_template(self):
        response = self.client.get("/")
        self.assertTemplateUsed(response, 'home.html')

    def test_can_save_a_POST_request(self):
        self.client.post('/', data={
            "ip_version" : "4",
            "probe_type" : "icmp_echo",
            "target" : "leboncoin.fr"}
        )

        self.assertEqual(Probe.objects.count(), 1)
        new_probe = Probe.objects.first()
        self.assertEqual(new_probe.ip_version, '4')
        self.assertEqual(new_probe.probe_type, 'icmp_echo')
        self.assertEqual(new_probe.target,  'leboncoin.fr')


    def test_redirect_after_POST(self):
        response = self.client.post('/', data={
            "ip_version" : "4",
            "probe_type" : "icmp_echo",
            "target" : "leboncoin.fr"}
        )
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/')
        

    def test_only_saves_probes_when_necesary(self):
        self.client.get('/')
        self.assertEqual(Probe.objects.count(), 0)
        

    def test_displays_all_probes(self):
        Probe.objects.create(
            ip_version="4",
            probe_type="icmp_echo",
            target="google.com"
        )   
        Probe.objects.create(
            ip_version="4",
            probe_type="icmp_echo",
            target="youtube.com"
        )

        response = self.client.get('/')

        self.assertIn('google.com', response.content.decode())
        self.assertIn('youtube.com', response.content.decode())
        
class ProbeViewTest(TestCase):

    def test_uses_view_probe_template(self):
        Probe.objects.create(
            ip_version = '4',
            probe_type = 'icmp_echo',
            target = 'google.com'
        )
        response = self.client.get('/probes/1')
        self.assertTemplateUsed(response, 'probe.html')

    
    def test_displays_probe_info(self):
        Probe.objects.create(
            ip_version = '4',
            probe_type = 'icmp_echo',
            target = 'google.com'
        )

        Probe.objects.create(
            ip_version = '4',
            probe_type = 'icmp_echo',
            target = 'netflix.com'
        )

        saved_probes = Probe.objects.all()
        self.assertEqual(saved_probes.count(), 2)

        response1 = self.client.get("/probes/1")
        self.assertContains(response1, "google.com")

        response2 = self.client.get("/probes/2")
        self.assertContains(response2, "netflix.com")
        

