from django.test import TestCase
from bacon.models import Probe

class ProbeModelTest(TestCase):

    def test_saving_and_retrieve_probes(self):
        first_probe = Probe()
        first_probe.ip_version = '4'
        first_probe.probe_type = 'icmp_echo'
        first_probe.target = 'google.com'
        first_probe.save()

        second_probe = Probe()
        second_probe.ip_version = '4'
        second_probe.probe_type = 'icmp_echo'
        second_probe.target = 'netflix.com'
        second_probe.save()

        saved_probes = Probe.objects.all()
        self.assertEqual(saved_probes.count(), 2)

        first_saved_probe = saved_probes[0]
        second_saved_probe = saved_probes[1]
        self.assertEqual(first_saved_probe.id, 1)
        self.assertEqual(first_saved_probe.ip_version, "4")
        self.assertEqual(first_saved_probe.probe_type, "icmp_echo")
        self.assertEqual(first_saved_probe.target, "google.com")
        self.assertEqual(second_saved_probe.id, 2)
        self.assertEqual(second_saved_probe.ip_version, "4")
        self.assertEqual(second_saved_probe.probe_type, "icmp_echo")
        self.assertEqual(second_saved_probe.target, "netflix.com")



