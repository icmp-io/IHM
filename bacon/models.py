from django.db import models
from django.contrib.auth.models import User

class UserJambon(models.Model):
  user = models.OneToOneField(User, on_delete=models.CASCADE)
  api_key = models.CharField(max_length=36)

class Probe(models.Model):
    id = models.AutoField(primary_key=True)
    ip_version = models.TextField(default='4')
    probe_type = models.TextField(default='icmp_echo')
    target = models.TextField(default='google.com')
    id_jambon = models.TextField(default="")
