from django.conf.urls import url
from bacon import views

urlpatterns = [
    url(r'^$', views.home_page, name="home"),
    url(r'^probes/(?P<id>[0-9]+)$', views.view_probe, name="view_probe")
    ]
