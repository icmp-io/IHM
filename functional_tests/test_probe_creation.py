from .base import FunctionalTest
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.keys import Keys
import unittest

class NewVisitorTest(FunctionalTest):
        
    def test_can_create_a_probe(self):
        # Joe has heard about a cool new availability monitoring App. He goes
        # to check its homepage
        self.browser.get(self.live_server_url)

        # He notices the page title and header mention Jambon is the new chicken
        self.assertIn('Jambon is the new chicken', self.browser.title)
        header_text = self.browser.find_element_by_tag_name("h1").text
        self.assertIn('Jambon is the new chicken', header_text)
        
        # He's invited to add a probe item straight away

        select_ip_version = self.browser.find_element_by_id('id_ip_version_select')  
        self.assertEqual(
            select_ip_version.get_attribute('name'),
            'ip_version'
        ) 
        select_probe_type = self.browser.find_element_by_id('id_probe_type_select')  
        self.assertEqual(
            select_probe_type.get_attribute('name'),
            'probe_type'
        )
        textinput_target = self.browser.find_element_by_id('id_target')  
        self.assertEqual(
            textinput_target.get_attribute('placeholder'),
            'Enter hostname or IP address'
        )
                
        # He fills the form with the following params:
        #   - "ip_version":"4"
        #   - "type":"icmp_echo"
        #   - "target":"google.com"


        ## We use JS here cause this fucking piece of shit of Webdriver doesn't work ! 
        self.browser.execute_script('document.getElementById("id_ip_version_select").selectedIndex = 0')

        ## Same as above
        self.browser.execute_script('document.getElementById("id_probe_type_select").selectedIndex = 0')
        textinput_target.send_keys("google.com")

        self.browser.find_element_by_id("id_submit").click()

        
        # When he hits enter, the page updates, and now the page displays
        # the probe attributes 
        self.wait_for_row_in_table("1 4 icmp_echo google.com")
        
        # There is still a form inviting him to add another probe
        # He fills the form with the following params:
        #   - "ip_version":"4"
        #   - "type":"icmp_echo"
        #   - "target":"netflix.com"
        self.browser.execute_script(
            'document.getElementById("id_ip_version_select").selectedIndex = 0')
        self.browser.execute_script(
            'document.getElementById("id_probe_type_select").selectedIndex = 0')
        textinput_target = self.browser.find_element_by_id('id_target')  
        textinput_target.send_keys("netflix.com")
        self.browser.find_element_by_id("id_submit").click()

        # The page updates again, and now shows both probes
        self.wait_for_row_in_table("1 4 icmp_echo google.com")
        self.wait_for_row_in_table("2 4 icmp_echo netflix.com")

    def test_can_view_probe_info(self):
        # Anakin start a new probe
        self.browser.get(self.live_server_url)
        textinput_target = self.browser.find_element_by_id('id_target')  
        self.browser.execute_script(
            'document.getElementById("id_ip_version_select").selectedIndex = 0')
        self.browser.execute_script(
            'document.getElementById("id_probe_type_select").selectedIndex = 0')
        textinput_target.send_keys("starkiller.com")

        self.browser.find_element_by_id("id_submit").click()

        # Then he decides he wants to see the info of his new probe
        probe_url = self.browser.current_url + "probes/3"
        self.browser.get(probe_url)
        
        self.assertRegex(self.browser.current_url, '/probes/3')  
        self.wait_for_row_in_table("3 4 icmp_echo starkiller.com")
        
