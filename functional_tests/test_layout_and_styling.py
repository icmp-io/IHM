from .base import FunctionalTest
from selenium.webdriver.common.keys import Keys

class LayoutAndStylingTest(FunctionalTest):

    def test_layout_and_styling(self):
        # Obi Wan goes to the home page
        self.browser.get(self.live_server_url)
        self.browser.set_window_size(1024, 768)

        # He notices the form is nicely centered
        form = self.browser.find_element_by_tag_name("form")
        self.assertAlmostEqual(form.location['x'] + form.size['width'] / 2,
                               512,
                               delta=10
                               )

