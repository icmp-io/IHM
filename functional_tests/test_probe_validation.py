from .base import FunctionalTest
from selenium.webdriver.common.keys import Keys


class ProbeValidationTest(FunctionalTest):

    def test_cannot_add_empty_probe(self):
        # Toto goes to the home page and accidentally tries to create an empty probe
        # He hits Enter on the emty form
        self.browser.get(self.live_server_url)

        textinput_target = self.browser.find_element_by_id('id_target')  
        textinput_target.send_keys(Keys.ENTER)

        # The home page refreshes, and there is an error message saying that the target
        # cannot be blank
        self.wait_for(lambda : self.assertEqual(
            self.browser.find_element_by_css_selector('.has-error').text,
            "You can't have an empty target for a probe"
        ))

        # He tries again with a hostname for the target, which now works
        textinput_target = self.browser.find_element_by_id('id_target')  
        textinput_target.send_keys("starkiller.com")
        textinput_target.send_keys(Keys.ENTER)
        self.wait_for_row_in_table("1 4 icmp_echo starkiller.com")
        
        # Perversevly, he now decides to submit a second blank target (what a jerk)
        textinput_target = self.browser.find_element_by_id('id_target')  
        textinput_target.send_keys(Keys.ENTER)
        
        # He tries receives a similar warning on the probes page
        self.wait_for(lambda : self.assertEqual(
            self.browser.find_element_by_css_selector('.has-error').text,
            "You can't have an empty target for a probe"
        ))

        
        # And he can correct it by filling some text in
        textinput_target = self.browser.find_element_by_id('id_target')  
        textinput_target.send_keys("google.com")
        textinput_target.send_keys(Keys.ENTER)
        self.wait_for_row_in_table("1 4 icmp_echo starkiller.com")
        self.wait_for_row_in_table("2 4 icmp_echo google.com")
        
